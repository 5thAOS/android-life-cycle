package com.example.rany.lifecycledemo;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "ooooo";
    TextView result;
    Button btnClick;
    EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, " On Create invoke");

        result = findViewById(R.id.textView2);
        username = findViewById(R.id.edtName);
        btnClick = findViewById(R.id.btnClick);

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userInput = username.getText().toString();
                result.setText(userInput);
            }
        });


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null){
            result.setText(savedInstanceState.getString("userInput"));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("userInput", username.getText().toString());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, " On Start invoke");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, " On Resume invoke");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, " On Pause invoke");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, " On Stop invoke");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, " On Destroy invoke");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, " On Restart invoke");
    }
}
